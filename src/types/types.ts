export enum FieldType {
  STRING = "string",
  NUMBER = "number",
}

export class Field {
  constructor(displayName: string, columnName: string, type: FieldType, placeholder: string) {
    this.displayName = displayName;
    this.columnName = columnName;
    this.type = type;
    this.placeholder = placeholder;
  }
  displayName: string;
  columnName: string;
  type: FieldType;
  placeholder: string;
}

export const fields: Field[] = [
  new Field("User Email", "user_email", FieldType.STRING, "johndoe@email.com"),
  new Field("Screen Width", "screen_width", FieldType.NUMBER, "0"),
  new Field("Screen Height", "screen_height", FieldType.NUMBER, "0"),
  new Field("# of Visits", "visits", FieldType.NUMBER, "0"),
  new Field("First Name", "user_first_name", FieldType.STRING, "John"),
  new Field("Last Name", "user_last_name", FieldType.STRING, "Doe"),
  new Field("Page Response time (ms)", "page_response", FieldType.NUMBER, "0"),
  new Field("Domain", "domain", FieldType.STRING, "website.com"),
  new Field("Page Path", "path", FieldType.STRING, "/cart"),
];

export class Operation {
  constructor(displayName: string, operator: string) {
    this.displayName = displayName;
    this.operator = operator;
  }
  displayName: string;
  operator: string;
}

export const stringOps: Operation[] = [
  new Operation("equals", "= '{0}'"),
  new Operation("contains", "like '%{0}%'"),
  new Operation("starts with", "like '{0}%'"),
  new Operation("in list", "in ({0})"),
];

export const numberOps: Operation[] = [
  new Operation("equals", "= {0}"),
  new Operation("between", "between {0} and {1}"),
  new Operation("greater than", "> {0}"),
  new Operation("less than", "< {0}"),
  new Operation("in list", "in ({0})"),
];

export class Predicate {
  constructor(field: Field) {
    this.field = field;
    if (field.type === FieldType.STRING) {
      this.operations = stringOps;
    } else {
      this.operations = numberOps;
    }
    this.selectedOp = this.operations[0];
    this.input1 = "";
    this.input2 = "";
  }
  field: Field;
  operations: Operation[];
  selectedOp: Operation;
  input1: string;
  input2: string;
}
