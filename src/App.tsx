import React, { useState, useEffect } from "react";
import { Predicate, fields } from "./types/types";
import PredicateForm from "./components/PredicateForm";
import "./styles/App.css";
import Button from "./components/Button";
import Sql from "./components/Sql";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";

function App() {
  const [predicates, setPredicates] = useState<Predicate[]>([new Predicate(fields[0])]);
  const [searching, setSearching] = useState(false);
  const [valid, setValid] = useState(false);

  useEffect(() => {
    let valid = !predicates.some((p) => !!!p.input1 || (p.selectedOp.displayName === "between" && !!!p.input2));
    if (valid) {
      const fields = predicates.map((p) => p.field.displayName);
      const set = new Set(fields);
      valid = set.size === fields.length;
    }
    setValid(valid);
  }, [predicates]);

  const addNewQuery = () => {
    const newPredicates = [...predicates, new Predicate(fields[0])];
    setPredicates(newPredicates);
    setSearching(false);
  };

  const onPredicateChange = (predicate: Predicate, index: number) => {
    const temp = predicates.map((p) => ({ ...p }));
    temp[index] = predicate;
    setPredicates(temp);
    setSearching(false);
  };

  const removePredicate = (index: number) => {
    const temp = predicates.map((p) => ({ ...p }));
    temp.splice(index, 1);
    if (temp.length === 0) {
      reset();
    } else {
      setPredicates(temp);
      setSearching(false);
    }
  };

  const reset = () => {
    setPredicates([new Predicate(fields[0])]);
    setSearching(false);
  };

  const search = () => {
    setSearching(true);
  };

  return (
    <div className="app">
      <h3 className="search-header">Search for Sessions</h3>
      {predicates.map((p, index) => (
        <PredicateForm
          key={index}
          predicate={p}
          index={index}
          onChange={onPredicateChange}
          removePredicate={removePredicate}
        ></PredicateForm>
      ))}
      <Button style={{ marginTop: "5px", marginBottom: "57px" }} size="sm" onClick={addNewQuery}>
        And
      </Button>
      <hr />
      <div className="button-container">
        <Button disabled={!valid} icon={<FontAwesomeIcon icon={faSearch} />} onClick={search}>
          Search
        </Button>
        <Button secondary onClick={reset}>
          Reset
        </Button>
      </div>
      {searching && <Sql predicates={predicates} />}
    </div>
  );
}

export default App;
