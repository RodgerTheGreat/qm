import React from "react";
import "../styles/Word.css";

interface Props {
  text: string;
}

const Word: React.FC<Props> = ({ text }) => {
  return <div className="word">{text}</div>;
};

export default Word;
