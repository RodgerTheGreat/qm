import React from "react";
import classnames from "classnames";
import "../styles/Button.css";

export type ButtonSize = "sm" | "md";

interface Props extends React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> {
  size?: ButtonSize;
  secondary?: boolean;
  icon?: JSX.Element;
}

const Button: React.FC<Props> = ({ size, secondary, children, icon, ...props }) => {
  return (
    <button
      className={classnames({
        "custom-button": true,
        "small-button": size === "sm",
        "secondary-button": secondary,
        "disabled-button": props.disabled,
        "icon-button": icon !== undefined,
      })}
      {...props}
    >
      <div className="button-content">
        {icon}
        {children}
      </div>
    </button>
  );
};

export default Button;
