import React from "react";
import classnames from "classnames";

interface Props {
  onClick: (value: string) => void;
  value: string;
  selected: boolean;
}

const SelectOption: React.FC<Props> = ({ onClick, value, selected }) => {
  const onSelect = () => {
    onClick(value);
  };

  return (
    <div className={classnames({ "select-option": true, selected: selected })} onClick={onSelect}>
      {value}
    </div>
  );
};

export default SelectOption;
