import React, { useState } from "react";
import SelectOption from "./SelectOption";
import classnames from "classnames";
import "../styles/CustomSelect.css";
import useComponentVisible from "./useComponentVisible";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown, faChevronUp } from "@fortawesome/free-solid-svg-icons";

export type SelectSize = "sm" | "md";

interface Props {
  onChange: (value: string) => void;
  value: string;
  options: string[];
  selectSize?: SelectSize;
}

const CustomSelect: React.FC<Props> = ({ onChange, value, options, selectSize }) => {
  const [open, setOpen] = useState(false);
  const { ref, isComponentVisible, setIsComponentVisible } = useComponentVisible(false);

  const onClick = () => {
    setOpen(!open);
    setIsComponentVisible(!open);
  };

  const onSelectionChanged = (value: string) => {
    setOpen(false);
    setIsComponentVisible(false);
    onChange(value);
  };

  return (
    <div
      className={classnames("custom-select", "custom-input", selectSize === "sm" ? "small-select" : "medium-select")}
    >
      <div className="select-header" onClick={onClick}>
        {value}
        <FontAwesomeIcon style={{ marginLeft: "auto" }} icon={isComponentVisible ? faChevronUp : faChevronDown} />
      </div>
      {open && isComponentVisible && (
        <div ref={ref} className="select-options">
          {options.map((o, i) => (
            <SelectOption selected={o === value} key={i} onClick={onSelectionChanged} value={o}></SelectOption>
          ))}
        </div>
      )}
    </div>
  );
};

export default CustomSelect;
