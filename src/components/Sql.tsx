import React from "react";
import { Predicate } from "../types/types";
import "../styles/Sql.css";

interface Props {
  predicates: Predicate[];
}

const Sql: React.FC<Props> = ({ predicates }) => {
  return (
    <div className="sql">
      <pre>
        <div>select * from sessions</div>
        <div>where</div>
        {predicates.map((p, i, preds) => (
          <div key={i}>
            {p.field.columnName} {p.selectedOp.operator.replace("{0}", p.input1).replace("{1}", p.input2)}
            {preds.length > 1 && i < preds.length - 1 && " and"}
            {i === preds.length - 1 && ";"}
          </div>
        ))}
      </pre>
    </div>
  );
};

export default Sql;
