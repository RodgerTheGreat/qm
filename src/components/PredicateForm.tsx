import React, { ChangeEvent, useState, useEffect } from "react";
import { Predicate, fields } from "../types/types";
import classnames from "classnames";
import Word from "./Word";
import "../styles/PredicateForm.css";
import CustomSelect from "./CustomSelect";

interface Props {
  predicate: Predicate;
  index: number;
  onChange: (predicate: Predicate, index: number) => void;
  removePredicate: (index: number) => void;
}

const PredicateForm: React.FC<Props> = ({ index, predicate, onChange, removePredicate }) => {
  const [multipleInputs, setMultipleInputs] = useState(false);

  useEffect(() => {
    setMultipleInputs(predicate.selectedOp.displayName === "between");
  }, [predicate.selectedOp.displayName]);

  const onFieldChange = (value: string) => {
    var field = fields.find(f => f.displayName === value);
    if (field) {
      onChange(new Predicate(field), index);
    }
  };

  const onOperationChange = (value: string) => {
    var op = predicate.operations.find(o => o.displayName === value);
    if (op) {
      onChange({ ...predicate, selectedOp: op }, index);
    }
  };

  const onInput1Change = (event: ChangeEvent<HTMLInputElement>) => {
    onChange({ ...predicate, input1: event.target.value }, index);
  };

  const onInput2Change = (event: ChangeEvent<HTMLInputElement>) => {
    onChange({ ...predicate, input2: event.target.value }, index);
  };

  const removeMe = () => {
    removePredicate(index);
  };

  return (
    <div className="predicate-form">
      <div className="x" onClick={removeMe}>
        ✕
      </div>

      <CustomSelect
        options={fields.map(f => f.displayName)}
        value={predicate.field.displayName}
        onChange={onFieldChange}
        selectSize={multipleInputs ? "sm" : "md"}
      />

      {multipleInputs && <Word text="is" />}

      <CustomSelect
        options={predicate.operations.map(o => o.displayName)}
        value={predicate.selectedOp.displayName}
        onChange={onOperationChange}
        selectSize={multipleInputs ? "sm" : "md"}
      />

      <input
        placeholder={predicate.field.placeholder}
        type={predicate.field.type.toString()}
        value={predicate.input1}
        onChange={onInput1Change}
        className={classnames("custom-input", multipleInputs ? "small-input" : "medium-input")}
      />

      {multipleInputs && (
        <>
          <Word text="and" />
          <input
            placeholder={predicate.field.placeholder}
            type={predicate.field.type.toString()}
            value={predicate.input2}
            onChange={onInput2Change}
            className={classnames("custom-input", multipleInputs ? "small-input" : "medium-input")}
          />
        </>
      )}
    </div>
  );
};

export default PredicateForm;
